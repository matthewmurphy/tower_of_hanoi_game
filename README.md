Tower of Hanoi Challenge:
// Collaboration of Chad Thompson & Matt Murphy //

The objective of the puzzle is to move the entire stack to another rod, obeying the following simple rules:

Only one disk can be moved at a time.
Each move consists of taking the upper disk from one of the stacks and placing it on top of another stack or on an empty rod.
No larger disk may be placed on top of a smaller disk.
With 4 disks, the puzzle can be solved in 15 moves.

-Boxes are flex-box containers.
-A single click will select the top most disk inside of a box.
-The second click will move the disk in to the selected box ONLY if 
-it is a legal move.
-When goal is achieved an alert box will appear and the user will need to refresh the page to 
play again 
