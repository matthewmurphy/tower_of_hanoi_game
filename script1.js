// Main Script for Tower of Hanoi //
// Purpose is to manipulate the discs to move until the challenge is complete //

const boxOne = document.querySelector('#boxOne'); // 3 flexbox selectors
const boxTwo = document.querySelector('#boxTwo');
const boxThree = document.querySelector('#boxThree');

let clickMode = true //Click Controller
let getDisc 
const youWin = function() {                     // Win Conditional
    if (boxThree.childElementCount === 4) {
        alert("!  !  Y O U   W I N  !  ! ((REFRESH PAGE TO PLAY AGAIN))")
    }
}

const moveDisc = function (evt) {
    const selectedBox = evt.currentTarget
    
        if (clickMode == true) {
            getDisc = selectedBox.lastElementChild
            clickMode = false
        }
        else if (clickMode == false) 
        {
            if (!selectedBox.firstElementChild) {
            selectedBox.appendChild(getDisc)
            return clickMode = true
            } else if (Number(getDisc.clientWidth) < Number(selectedBox.lastElementChild.clientWidth)) {
            selectedBox.appendChild(getDisc)
            youWin();
            return clickMode = true
            } else {
            return clickMode = true
            }   
        }
        
          
    console.log(clickMode)
}

boxOne.addEventListener('click', moveDisc)
boxTwo.addEventListener('click', moveDisc)
boxThree.addEventListener('click', moveDisc)
